const express = require('express')
const mongoose = require('mongoose')
const nodemailer = require('nodemailer')
const path = require('path')
const app = express()
const port = 3000

const FORM = require('./models/form') //model para la estructura del form
const SUBMIT = require('./models/submits') //model para la informacion de los submits

//config:
const FORM_TITLE = 'Semana del software libre' //titulo del form con el que vamos a trabajar (campo title de la query que hicimos al principio a mano)
var mongoDB = 'mongodb://127.0.0.1/open_forms'; //uri de mongo
const base_url = 'http://localhost:3000' //url base
const production = true;
  // modo produccion:
  // no hace logs, activa regex y no permite que se repita el mail
//end of config

app.set('view engine', 'pug');

app.use(express.urlencoded({ extended: true }));

app.use(express.static(path.join(__dirname, 'public')))

var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'dvd.loge@gmail.com',
    pass: 'tu contrasenia wapa'
  }
});

mongoose.connect(mongoDB, {useNewUrlParser: true})

// Get Mongoose to use the global promise library
// mongoose.Promise = global.Promise;
//Get the default connection
var db = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

//home page

app.get('/', (req, res) => {

  FORM.findOne({'title': FORM_TITLE}, (err, ok) => {
    if(err) console.log(err);
    else {
        if(!production) console.log(ok)
        res.render('home', {form:ok, mode:production});
      }
    })
})

app.post('/', function (req, res){
  if(!production)console.log(req.body);
  let regex = /.*\@(?:.*\.)?ucm\.es/
  let match = regex.exec(req.body.email)
  if(match !== null){
    SUBMIT.countDocuments({'email':req.body.email}, (err, count) => {
      if(err) console.log(err)
      else{
        if(count == 0){
          //puedo insertarlo y mandar el correo
          let key = hashIt();
          let options = {
            data:[{facultad:req.body.facultad}, {charla:req.body.charla}]
          }
          let query = new SUBMIT({
            email: req.body.email,
            options: options.data,
            key: key,
            validado: false
          });
          query.save().then((err, ok) => {
            if(!production){
              console.log(ok)
            }
            var mailOptions = {
              from: 'dvd.loge@gmail.com',
              to: req.body.email,
              subject: FORM_TITLE + ' validation',
              html: '<h1>Validar voto</h1><p>Haz click en el link para validar tu voto<br><a href='+base_url+'/validateVote/'+req.body.email+'/'+key+'>link para validar</a></p>'
            };
            transporter.sendMail(mailOptions, function(error, info){
              if (error) {
                console.log(error);
              } else {
                console.log('Email sent: ' + info.response);
              }
            });
            res.render('about', {msg:{
              color: 'teal',
              status: 'Enviado con exito',
              description: 'Tu informacion se ha almacenado con exito, sin embargo para que tu voto cuente como valido, te hemos enviado un link al correo que nos has proporcionado, corre a verlo, si tienes algun problema no dudes en contactar con nosotros'
            }})
          })


        }else{
          //el correo ya esta registrado...
          res.render('about', {msg:{
            color: 'amber',
            status: 'Correo ya registrado',
            description: 'Ya hemos recibido un voto con este correo, si tienes algun problema no dudes en contactar con nosotros'
          }})

        }
      }
    })
  }else{
    //enviar error de correo ucm...
    res.render('about', {msg:{
      color: 'amber',
      status: 'Tipo de correo no valido',
      description: 'El formato de tu correo no era valido, recuerda que tiene que ser @ucm.es, para cualquier duda ponte en contacto con nosotros'
    }})
  }
})

app.get('/validateVote/:mail/:hash', (req, res) => {
  SUBMIT.findOne({email:req.params.mail}, (err, ok) => {
    let msg;
    if(err){
      console.log(err)
    }
    else{
      if(ok.key == req.params.hash && req.params.hash != 'KEY_USADA'){
        ok.validado = true
        ok.key = 'KEY_USADA'
        ok.save()
        msg = 'Tu voto se ha validado correctamente, gracias por usar open forms :)'
      }else{
        if(ok.validado == true) {
          msg = 'Algo ha ido mal, tu voto ya se ha validado, ponte en contacto con nosotros si crees que hay un mal funcionamiento'
        }else{
          msg = 'Algo ha ido mal, tu voto no se ha podido validar, ponte en contacto con nosotros si crees que hay un mal funcionamiento'

        }
      }
      res.render('validateVote', {msg:msg})
    }
  })

});

app.get('/about', (req, res) => {
  res.render('about')
})

app.get('/admin/:pass', (req, res) => {
  FORM.findOne({title:FORM_TITLE}, (err, form) => {
    if(err) console.log(err)
    else{
      if(form.admin == req.params.pass){
        SUBMIT.find({}, (err, submits) => {
          // console.log(submits);
          res.render('admin', {submits:submits, pass:req.params.pass})
        })
      }else{
        res.send('No te pases de listo...')
      }
    }
  })
})

app.post('/delete', (req, res) => {
  SUBMIT.deleteOne({_id:req.body.id}, (err) => {
    if(err) console.log(err)
    else{
      res.redirect('/admin/'+req.body.pass);
    }
  })
})
function hashIt(){
  let length = Math.floor(Math.random() * 6 + 5)
  let possible = 'abcdefghikjlmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
  let result = '';
  for(let i = 0; i < length; i++){
    result += possible[Math.floor(Math.random() * (possible.length + 1))]
  }
  return result;
}





app.listen(port, () => console.log(`Example app listening on port ${port}!`))
