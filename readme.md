# OPEN FORMS
### COMO FUNCIONA
*se intuye un entorno unix y se debe tener instalados node y mongo*
1. ```npm install```
2. en una consola iniciar mongo y crear una base de datos llamada open_forms (se puede cambiar el nombre si se cambia tambien en el index.js en la url de conexion de mongo)
3. ```use open_forms```
4. ```
   db.forms.insert({
    title:'Semana del software libre',
    admin:'algunacontraseniasuperchungui',

     structure: 
     [
      [ 'fisica', 'mates', 'biologia', 'ccinfo', 'derecho' ],
      [ 'anonimato en la red',
        'peligros en la actualidad',
        'hacking etico con wifi',
        'stalkeo en redes sociales',
        'hacking de juegos',
        'herramientas libres de ofimatica',
        'protegete de los macro virus',
        'crea tu web personal',
        'computacion cuantica',
        'aprende a progamar',
        'tratamiento de datos',
        'bots de twitter(replicantes)',
        'bots de telegram ',
        'scraping web',
        'automatizacion de tareas cotidianas',
        'inteligencia artificial',
        'inteligencia artificial aplicada a la sociedad',
        'sesgos en los algoritmos',
        'IOT, haz tu casa del futuro',
        'juegos libres',
        'ciberacoso escolar',
        'privacidad, identidad digital y reeputacion',
        'netiqueta: comportamiento en linea',
        'gestion de la informacion y acceso a contenidos inapropiados',
        'proteccion ante virus y fraudes',
        'uso excesivo de las tic',
        'mediacion parental y uso de herramientaas de control parental',
        'uso seguro y responsable de las tic' ] 
     ]

   })```
Este es solo un ejemplo, puede encontrarse en el archivo db.json , pero lo que es importante tener en cuenta es que deberemos meter estos datos a mano desde la shell de mongo (*proximamamente desde interfaz de la propia aplicacion*)

5. Debemos tener en cuenta el titulo que le hayamos puesto en el paso anterior en el cmapo title, pues es el que tendremos que poner en la constante FORM_TITLE en el archivo index.js para que encuentre la base de datos correctamente. Esto nos permite reutilizar la aplicacion para diferentes bases de datos
6. ya deberia estar todo listo, ejecutamos ```npm run test```
7. Hay que tener en cuenta que hay dos modos, el de produccion y el de development
  * En produccion no hace logs(solo los minimos), activa el regex* para el correo y no permite que se repite el email en la base de datos. Actualmente regex esta activo siempre, permite verificar que un correo viene de un dominio concreto
8. Entramos en localhost:3000 (o donde se haya configurado) e introducimos datos
9. Una vez relleno le damos a submit, aparecera un toast con la respuesta, si todo ha ido bien se habra enviado un mail a la direccion escrita.
  * Para que funcione el servicio de mail, hemos de poner nuestra propia cuenta en la variable transporter de index.js
10. Cuando recibimos el mail, deberiamos ver un link, pinchando en el nos lleva a una pagina que nos muestra informacion sobre si se ha validado nuestro voto correctamente o un mensaje de error descriptivo en otro caso
11. Se pueden visualizar los datos de forma un poco tosca en la pestania /admin/lacontraseniaquehayaspuestoenelcampoadmin , estamos mejorando poco a poco :)
12. No te olvides de cambiar el base_url, ya que es necesario para los correos, o sino habra fallos

> Si quieres hacer algo, hazlo tú mismo, no esperes que otros lo hagan por ti

**Esta aplicacion esta desarrollada por miembros de LibreLabUCM**
