const mongoose = require('mongoose')

const SubmitSchema = new mongoose.Schema({
  email:{
    type:String,
    required:true
  },
  options:{
    type:Array,
    required:true
  },
  validado:{
    type:Boolean,
    required:true
  },
  key:{
    type:String,
    required:true
  }
})

const Submit = mongoose.model('submit', SubmitSchema);

module.exports = Submit
