const mongoose = require('mongoose')

const FormSchema = new mongoose.Schema({
  title:{
    type:String,
    required:true
  },
  structure:{
    type:Array,
    required:true
  },
  admin:{
    type:String
  }
})

const Form = mongoose.model('form', FormSchema);

module.exports = Form
